# == Route Map
#
#                   Prefix Verb   URI Pattern                    Controller#Action
#         new_user_session GET    /users/sign_in(.:format)       devise/sessions#new
#             user_session POST   /users/sign_in(.:format)       devise/sessions#create
#     destroy_user_session DELETE /users/sign_out(.:format)      devise/sessions#destroy
#            user_password POST   /users/password(.:format)      devise/passwords#create
#        new_user_password GET    /users/password/new(.:format)  devise/passwords#new
#       edit_user_password GET    /users/password/edit(.:format) devise/passwords#edit
#                          PATCH  /users/password(.:format)      devise/passwords#update
#                          PUT    /users/password(.:format)      devise/passwords#update
# cancel_user_registration GET    /users/cancel(.:format)        devise/registrations#cancel
#        user_registration POST   /users(.:format)               devise/registrations#create
#    new_user_registration GET    /users/sign_up(.:format)       devise/registrations#new
#   edit_user_registration GET    /users/edit(.:format)          devise/registrations#edit
#                          PATCH  /users(.:format)               devise/registrations#update
#                          PUT    /users(.:format)               devise/registrations#update
#                          DELETE /users(.:format)               devise/registrations#destroy
#                    posts GET    /posts(.:format)               posts#index
#                          POST   /posts(.:format)               posts#create
#                 new_post GET    /posts/new(.:format)           posts#new
#                edit_post GET    /posts/:id/edit(.:format)      posts#edit
#                     post GET    /posts/:id(.:format)           posts#show
#                          PATCH  /posts/:id(.:format)           posts#update
#                          PUT    /posts/:id(.:format)           posts#update
#                          DELETE /posts/:id(.:format)           posts#destroy
#             person_index GET    /person(.:format)              person#index
#                          POST   /person(.:format)              person#create
#               new_person GET    /person/new(.:format)          person#new
#              edit_person GET    /person/:id/edit(.:format)     person#edit
#                   person GET    /person/:id(.:format)          person#show
#                          PATCH  /person/:id(.:format)          person#update
#                          PUT    /person/:id(.:format)          person#update
#                          DELETE /person/:id(.:format)          person#destroy
#                    twits GET    /twits(.:format)               twits#index
#                          POST   /twits(.:format)               twits#create
#                 new_twit GET    /twits/new(.:format)           twits#new
#                edit_twit GET    /twits/:id/edit(.:format)      twits#edit
#                     twit GET    /twits/:id(.:format)           twits#show
#                          PATCH  /twits/:id(.:format)           twits#update
#                          PUT    /twits/:id(.:format)           twits#update
#                          DELETE /twits/:id(.:format)           twits#destroy
#            relationships POST   /relationships(.:format)       relationships#create
#             relationship DELETE /relationships/:id(.:format)   relationships#destroy
#                     root GET    /                              redirect(301, /person)
#

Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  devise_for :users, :controllers => { omniauth_callbacks: 'omniauth_callbacks' }
  resources :posts
  resources :person
  resources :twits

  resources :relationships, only: [:create, :destroy]

  root :to => redirect('/person')

  match '/users/:id/finish_signup' => 'person#finish_signup', via: [:get, :patch], :as => :finish_signup
  # get '/person/:id/followers' => 'person#followers'
  # get '/person/:id/following' => 'person#following'

  # get '/followers/:id' => 'followers#index'

  match '/followers/:id' => 'followers#index', via: :get, :as => :followers
  match '/following/:id' => 'following#index', via: :get, :as => :following

end