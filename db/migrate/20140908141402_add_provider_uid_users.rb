class AddProviderUidUsers < ActiveRecord::Migration
  def change
    change_table(:users) do |t|
      # Confirmable
      t.string   :provider
      t.string   :uid
    end
    add_index  :users, :uid, :unique => true
  end
end