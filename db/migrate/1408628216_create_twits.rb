class CreateTwits < ActiveRecord::Migration
  def change
    create_table :twits do |t|
      t.string :content
      t.integer :user_id

      t.timestamps
    end
    add_index :twits, [:user_id, :created_at]
  end
end