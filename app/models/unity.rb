# == Schema Information
#
# Table name: unities
#
#  id         :integer          not null, primary key
#  role_id    :integer
#  user_id    :integer
#  created_at :datetime
#  updated_at :datetime
#

class Unity < ActiveRecord::Base



  belongs_to :role
  belongs_to :user

  def unity_params
    params.require(:unity).permit(:role_id, :user_id)
  end

end
