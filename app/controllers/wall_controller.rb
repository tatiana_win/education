class WallController < ApplicationController

  def show
    @user = User.find(params[:id])
    @twits = @user.twits.paginate(page: params[:page])
  end

end
