class RegistrationController < Devise::RegistrationsController
  private

  def sign_up_params
    params.require(:user).permit(:login, :email, :password, :password_confirmation)
  end

  def account_update_params
    params.require(:user).permit(:login, :email, :password, :password_confirmation, :current_password)
  end

  def new
    super
  end

  def create
    # add custom create logic here
  end

  def update
    super
  end
end
