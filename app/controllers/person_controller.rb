class PersonController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy]

  helper_method :following, :followers

  def show
    @user = User.find(params[:id])
    @twit = @user.twits.build if signed_in?
    @twits = @user.twits.paginate(page: params[:page])
    @host = request.host_with_port()
  end

  def index
    @host = request.host_with_port()
    if(Unity.where(user_id: current_user.id).first)
      current_user.current_role = Unity.where(user_id: current_user.id).first.role.name
    else
      current_user.current_role = ""
    end
    @user = current_user
    @twit = current_user.twits.build if signed_in?
    @twits = current_user.feed
    @twits = @twits.paginate(page: params[:page], :per_page => 2)
    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @twits }
    end

  end

  def following
    @title = "Following"
    @user = User.find(params[:id])
    @users = @user.followed_users.paginate(page: params[:page])
    render 'show_follow'
  end

  def followers
    @title = "Followers"
    @user = User.find(params[:id])
    @users = @user.followers.paginate(page: params[:page])
    render 'show_follow'
  end

  def finish_signup
    # authorize! :update, @user
    if request.patch? && params[:user] #&& params[:user][:email]
      if current_user.update(user_params)
        current_user.skip_reconfirmation!
        sign_in(current_user, :bypass => true)
        redirect_to root_url, notice: 'Your profile was successfully updated.'
      else
        @show_errors = true
      end
    end
  end

  private
  def set_user
    @user = User.find(params[:id])
  end

  def user_params
    accessible = [ :name, :email ] # extend with your own params
    accessible << [ :password, :password_confirmation ] unless params[:user][:password].blank?
    params.require(:user).permit(accessible)
  end
end
