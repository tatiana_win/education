class RelationshipsController < ApplicationController

  def create
    @relationship = current_user.relationships.build(relationships_params)
    if @relationship.save
      redirect_to(:back)
    else
      redirect_to(:back)
    end
  end

  def destroy
    @relationship = Relationship.find(params[:id])
    if @relationship.destroy
      redirect_to(:back)
    else
      redirect_to(:back)
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_relationship
    @relationship = Relationship.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def relationships_params
    params.require(:relationship).permit(:followed_id, :follower_id)
  end
end
