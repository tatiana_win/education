class TwitsController < ApplicationController

  before_action :correct_user,   only: :destroy

  def index
    @twits = Twit.all
  end

  # GET /posts/1
  # GET /posts/1.json
  def show
  end

  # GET /posts/new
  def new
    @twit = Twit.new
  end

  def update
    @twit.update(twits_params)
  end

  # GET /posts/1/edit
  def edit
  end

  def create
    @twit = current_user.twits.build(twits_params)
    if @twit.save
      flash[:success] = "Twit created!"
      redirect_to root_url
    else
      @feed_items = []
      render 'person/index'
    end
  end

  def destroy
    if @twit.destroy
      redirect_to root_url
    end
  end

  def self.from_users_followed_by(user)
    followed_user_ids = 'SELECT followed_id FROM relationships WHERE follower_id = :user_id'
    where("user_id IN (#{followed_user_ids}) OR user_id = :user_id",
          user_id: user.id)
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_twit
    @twit = Twit.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def twits_params
    params.require(:twit).permit(:content, :user_id)
  end

  def correct_user
    @twit = current_user.twits.find(params[:id])
  rescue
    redirect_to root_url
  end


end
