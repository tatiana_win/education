class FollowingController < ApplicationController
  def index
    @title = "Following"
    @user = User.find(params[:id])
    @users = @user.followed_users.paginate(page: params[:page])

  end
end
