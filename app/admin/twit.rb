ActiveAdmin.register Twit do

  # controller.authorize_resource
  controller do
      def permitted_params
        params.permit twit: [ :content, :user_id ]
      end
    end

  # controller do
  #   def create
  #     params[:customer][:franchise_id] = current_user.franchise_id if franchise_user?
  #     create!
  #   end
  # end


  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if resource.something?
  #   permitted
  # end


end
